package services;

import models.CasaDeMarcat;
import models.Client;

import java.util.Random;

public class ClientGenerator{

    private Client client;
    private CasaDeMarcat casaDeMarcat;

    public ClientGenerator(Client client, CasaDeMarcat casaDeMarcat) {
        this.client = client;
        this.casaDeMarcat = casaDeMarcat;
    }


    public int arrivalTime(){
        Random random = new Random();
        return random.nextInt(client.getMaxArrivalTime());
    }

}
