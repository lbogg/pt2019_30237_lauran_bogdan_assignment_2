package services;

import models.CasaDeMarcat;
import models.Client;

import java.util.ArrayList;
import java.util.List;

import static java.lang.Thread.sleep;

public class MagazinService implements Runnable {

    private List<CasaDeMarcat> casaDeMarcatList;
    private int nrCase;
    static int id = 0;
    private int nrClienti;
    private int serviceTime;
    private int simulationTime;
    private int maxArrivalTime;
    private int minArrivalTime;
    private int waitTime;
    private ClientGenerator clientGenerator;

    public MagazinService(int nrCase, int nrClienti, int serviceTime, int simulationTime, int maxArrivalTime, int minArrivalTime, int waitTime) {
        this.nrCase = nrCase;
        casaDeMarcatList = new ArrayList<>();
        this.nrClienti = nrClienti;
        this.serviceTime = serviceTime;
        this.simulationTime = simulationTime;
        this.maxArrivalTime = maxArrivalTime;
        this.minArrivalTime = minArrivalTime;
        this.waitTime = waitTime;

        for (int i = 1; i <= nrCase; i++){
            CasaDeMarcat casaDeMarcat = new CasaDeMarcat(i,serviceTime);
            casaDeMarcatList.add(casaDeMarcat);
            Thread thread = new Thread(casaDeMarcat);
            thread.start();
        }
    }

    private int minIndex(){
        int index = 0;
        try {
            int min = casaDeMarcatList.get(0).lungimeCoada();
            for(int i = 1; i <= nrCase; i++){
                int size = casaDeMarcatList.get(i).lungimeCoada();
                if(size < min){
                    min = size;
                    index = i;
                }
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return index;
    }

    public void run(){
        try{
            int i = 0;
            while(i < nrClienti){
                i++;
                Client client = new Client(i,minArrivalTime,maxArrivalTime,waitTime);
                int m = minIndex();
                System.out.println("Clientul: "+client.getId() + " se pune la casa" + m);
                casaDeMarcatList.get(m).addClient(client);
                sleep(clientGenerator.arrivalTime());
            }
        }catch (InterruptedException e){
            e.printStackTrace();
        }
    }
}
