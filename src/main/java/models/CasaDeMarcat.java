package models;


import java.util.Collection;
import java.util.Iterator;
import java.util.Queue;

import static java.lang.Thread.sleep;

public class CasaDeMarcat implements Runnable {

   private Queue<Client> coada;
   private int id;
   private int serviceTime;

    public CasaDeMarcat(int id, int serviceTime) {
        this.id = id;
        this.serviceTime = serviceTime;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getServiceTime() {
        return serviceTime;
    }

    public void setServiceTime(int serviceTime) {
        this.serviceTime = serviceTime;
    }

    public void run(){
        try{
            while(true){
                sleep(serviceTime);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public synchronized void addClient(Client client) throws InterruptedException{
        coada.add(client);
        notifyAll();
    }

    public synchronized void deleteClient() throws InterruptedException{
        while(coada.isEmpty()){
            wait();
        }
        Client c = coada.remove();
        System.out.println(c.getId()+" a parasit casa cu numarul "+ getId());
        notifyAll();
    }

    public synchronized int lungimeCoada() throws InterruptedException{
        notifyAll();
        int size = coada.size();
        return size;
    }
}
